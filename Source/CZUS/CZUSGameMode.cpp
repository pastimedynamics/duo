// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "CZUS.h"
#include "CZUSGameMode.h"
#include "CZUSCharacter.h"

ACZUSGameMode::ACZUSGameMode()
{
	// set default pawn class to our character
	DefaultPawnClass = ACZUSCharacter::StaticClass();	
}
